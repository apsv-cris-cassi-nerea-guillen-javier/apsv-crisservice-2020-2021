package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Publication;

public interface PublicationDAO {
	Publication create( Publication Publication );
	Publication read( String PublicationId );
	Publication update( Publication Publication );
	Publication delete( Publication Publication );

	List<Publication> readAll();
	List<Publication> readAllPublications(String researcherId);

}



